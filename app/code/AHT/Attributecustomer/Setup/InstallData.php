<?php
namespace AHT\Attributecustomer\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory, Config $eavConfig, \Magento\Customer\Model\ResourceModel\Attribute $attributeResource)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig       = $eavConfig;
        $this->attributeResource = $attributeResource;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $attributesInfo = [
            'organization_name' => [
                'type'         => 'text',
				'label'        => 'Organization Name',
				'input'        => 'text',
				'required'     => false,
				'visible'      => true,
				'user_defined' => true,
				'position'     => 999,
				'system'       => 0,
            ],
            'phone_number' => [
                'type'         => 'varchar',
				'label'        => 'Phone Number',
				'input'        => 'text',
				'required'     => true,
				'visible'      => true,
				'user_defined' => true,
				'position'     => 999,
				'system'       => 0,
            ],
            'company_type' => [
                'type'         => 'int',
				'label'        => 'Company Type',
				'input'        => 'select',
                'source'       => 'AHT\Attributecustomer\Model\Source\CompanyTypeDropdown',
				'required'     => true,
				'visible'      => true,
				'user_defined' => true,
				'position'     => 999,
				'system'       => 0,
            ],
        ];

        foreach ($attributesInfo as $attributeCode => $attributeParams) {
            $eavSetup->removeAttribute(
                \Magento\Customer\Model\Customer::ENTITY,
                $attributeCode
            );

            $eavSetup->addAttribute(
                \Magento\Customer\Model\Customer::ENTITY,
                $attributeCode,
                $attributeParams
            );

            $attributeSetId = $eavSetup->getDefaultAttributeSetId(\Magento\Customer\Model\Customer::ENTITY);
            $attributeGroupId = $eavSetup->getDefaultAttributeGroupId(\Magento\Customer\Model\Customer::ENTITY);
            $attribute = $this->eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, $attributeCode);
            $attribute->setData('attribute_set_id', $attributeSetId);
            $attribute->setData('attribute_group_id', $attributeGroupId);
            $attribute->setData('used_in_forms', [
                'adminhtml_customer', 'checkout_register', 'customer_account_create', 'customer_account_edit', 'adminhtml_checkout'
            ]);
            
            $this->attributeResource->save($attribute);
        }

        $setup->endSetup();
    }
}