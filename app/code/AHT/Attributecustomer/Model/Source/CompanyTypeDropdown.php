<?php
namespace AHT\Attributecustomer\Model\Source;

class CompanyTypeDropdown extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    public function getAllOptions() {
        if ($this->_options === null) {
            $this->_options = [
                ['label' => __('--Select--'), 'value' => ''],
                ['label' => __('CBD Manufacturer'), 'value' => 1],
                ['label' => __('CBD Brand and Marketing Company'), 'value' => 2],
                ['label' => __('CBD Extractor'), 'value' => 3],
                ['label' => __('Other'), 'value' => 4]
            ];
        }
        return $this->_options;
    }
}
