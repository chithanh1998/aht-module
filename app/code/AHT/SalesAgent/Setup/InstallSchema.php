<?php
namespace AHT\SalesAgent\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('aht_sales_agent'))
            ->addColumn('entity_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Id')
            ->addColumn('order_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => false], 'Order Id')
            ->addColumn('order_item_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => false], 'Order Item Id')
            ->addColumn('order_item_sku', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, ['nullable' => false], 'Order Item SKU')
            ->addColumn('order_item_price', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', ['nullable' => false, 'default' => '0.00'], 'Order Item Price')
            ->addColumn('commission_percent', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => true], 'Commission Percent')
            ->addColumn('commission_value', \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '10,2', ['nullable' => false, 'default' => '0.00'], 'Commission Value')
            ->setComment('Table comment');
        $installer->getConnection()->createTable($table);

        $installer->endSetup();

    }
}
