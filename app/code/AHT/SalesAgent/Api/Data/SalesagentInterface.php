<?php
namespace AHT\SalesAgent\Api\Data;

interface SalesagentInterface
{
    const ENTITY_ID = 'entity_id';
    const ORDER_ID = 'order_id';
    const ORDER_ITEM_ID = 'order_item_id';
    const ORDER_ITEM_SKU = 'order_item_sku';
    const ORDER_ITEM_PRICE = 'order_item_price';
    const COMMISSION_VALUE = 'commission_value';
    const COMMISSION_PERCENT = 'commission_percent';

    /**
	 * Get entity id
	 *
	 * @return int|null
	 */
	public function getEntityId();

    /**
	 * Get order id
	 *
	 * @return int|null
	 */
	public function getOrderId();

    /**
	 * Get order item id
	 *
	 * @return int|null
	 */
	public function getOrderItemId();

    /**
	 * Get order item sku
	 *
	 * @return string|null
	 */
	public function getOrderItemSku();

    /**
	 * Get order item price
	 *
	 * @return float|null
	 */
	public function getOrderItemPrice();

    /**
	 * Get commission percent
	 *
	 * @return int|null
	 */
	public function getCommissionPercent();

    /**
	 * Get commission value
	 *
	 * @return float|null
	 */
	public function getCommissionValue();

    /**
	 * Set entity id
	 *
	 * @return int|null
	 */
	public function setEntityId($entity_id);

    /**
	 * Set order id
	 *
	 * @return int|null
	 */
	public function setOrderId($order_id);

    /**
	 * Set order item id
	 *
	 * @return int|null
	 */
	public function setOrderItemId($order_item_id);

    /**
	 * Set order item sku
	 *
	 * @return text|null
	 */
	public function setOrderItemSku($order_item_sku);

    /**
	 * Set order item price
	 *
	 * @return decimal|null
	 */
	public function setOrderItemPrice($order_item_price);

    /**
	 * Set commission percent
	 *
	 * @return int|null
	 */
	public function setCommissionPercent($percent);

    /**
	 * Set commission value
	 *
	 * @return decimal|null
	 */
	public function setCommissionValue($value);
}
