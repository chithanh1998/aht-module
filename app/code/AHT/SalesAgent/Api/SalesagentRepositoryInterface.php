<?php
namespace AHT\SalesAgent\Api;

interface SalesagentRepositoryInterface
{
    /**
     * Undocumented function
     *
     * @param \AHT\SalesAgent\Api\Data\SalesagentInterface $salesagent
     * @return \AHT\SalesAgent\Api\Data\SalesagentInterface
     */
    public function save(\AHT\SalesAgent\Api\Data\SalesagentInterface $salesagent);
    

    /**
     * Undocumented function
     *
     * @param int $entityId
     * @return \AHT\SalesAgent\Api\Data\SalesagentInterface
     */
    public function getById($entityId);

    /**
     * Undocumented function
     *
     * @param \AHT\SalesAgent\Api\Data\SalesagentInterface $salesagent
     * @return \AHT\SalesAgent\Api\Data\SalesagentInterface
     */
    public function delete(\AHT\SalesAgent\Api\Data\SalesagentInterface $salesagent);

    /**
     * Undocumented function
     *
     * @param  string $entityId
     * @return \AHT\SalesAgent\Api\Data\SalesagentInterface
     */
    public function deleteById($entityId);
}
