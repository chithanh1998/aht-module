<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Report Sold Products collection
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
namespace AHT\SalesAgent\Model\ResourceModel\Product\Sold;

use Magento\Framework\DB\Select;
use Zend_Db_Select_Exception;

/**
 * Data collection.
 *
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 * @api
 * @since 100.0.2
 */
class Collection extends \Magento\Reports\Model\ResourceModel\Order\Collection
{
    /**
     * Set Date range to collection.
     *
     * @param string $from
     * @param string $to
     * @return $this
     * @throws Zend_Db_Select_Exception
     */
    public function setDateRange($from, $to)
    {
        $this->_reset()->addAttributeToSelect(
            '*'
        )->addOrderedQty(
            $from,
            $to
        )->setOrder(
            'ordered_qty',
            self::SORT_ORDER_DESC
        );
        return $this;
    }

    /**
     * Add ordered qty's
     *
     * @param string $from
     * @param string $to
     * @return $this
     * @throws Zend_Db_Select_Exception
     */
    public function addOrderedQty($from = '', $to = '')
    {
        $connection = $this->getConnection();
        $orderTableAliasName = $connection->quoteIdentifier('order');

        $orderJoinCondition = [
            $orderTableAliasName . '.entity_id = order_items.order_id',
            $connection->quoteInto("{$orderTableAliasName}.state <> ?", \Magento\Sales\Model\Order::STATE_CANCELED),
        ];

        if ($from != '' && $to != '') {
            $fieldName = $orderTableAliasName . '.created_at';
            $orderJoinCondition[] = $this->prepareBetweenSql($fieldName, $from, $to);
        }

        $this->getSelect()->reset()->from(
            ['order_items' => $this->getTable('sales_order_item')],
            [
                'ordered_qty' => 'order_items.qty_ordered',
                'order_items_name' => 'order_items.name',
                'order_items_sku' => 'order_items.sku'
            ]
        )->joinInner(
            ['order' => $this->getTable('sales_order')],
            implode(' AND ', $orderJoinCondition),
            []
            )->joinLeft( 
                ['commission_value' => $this->getConnection()->getTableName('catalog_product_entity_decimal')],
                'order_items.product_id = commission_value.entity_id and commission_value.attribute_id = "175"',
                ['sa_commission_value' => 'commission_value.value']
            )
            ->joinLeft( 
                ['commission_type' => $this->getConnection()->getTableName('catalog_product_entity_int')],
                'order_items.product_id = commission_type.entity_id and commission_type.attribute_id = "174"',
                ['sa_commission_type' => 'commission_type.value',]
            )
            ->joinLeft( 
                ['sale_agent' => $this->getConnection()->getTableName('catalog_product_entity_int')],
                'order_items.product_id = sale_agent.entity_id and sale_agent.attribute_id = "173"',
                [   'sale_agent' => 'sale_agent.value',
                    ]
            )
            ->joinLeft(
                ['sale_agent_name' => $this->getConnection()->getTableName('customer_entity')],
                "sale_agent.value = sale_agent_name.entity_id ",
                [
                    'saleagent_name' => 'CONCAT(sale_agent_name.lastname," ", sale_agent_name.firstname )',
                ]
            )->where(
            'order_items.parent_item_id IS NULL and sale_agent.value IS NOT NULL'
        )->having(
            'order_items.qty_ordered > ?',
            0
        )->columns(
            'SUM(order_items.qty_ordered) as ordered_qty'
        )
        ->columns(
            'order_items.base_row_total_incl_tax as product_price_final'
        )
        ->columns(
            '(order_items.base_price*commission_value.value/100) as result_commission'
        )->group(
            'order_items.sku'
        );
        return $this;
    }

    /**
     * Set store filter to collection
     *
     * @param array $storeIds
     * @return $this
     */
    public function setStoreIds($storeIds)
    {
        if ($storeIds) {
            $this->getSelect()->where('order_items.store_id IN (?)', (array)$storeIds);
        }
        return $this;
    }

    /**
     * Set order
     *
     * @param string $attribute
     * @param string $dir
     * @return $this
     */
    public function setOrder($attribute, $dir = self::SORT_ORDER_DESC)
    {
        if (in_array($attribute, ['orders', 'ordered_qty'])) {
            $this->getSelect()->order($attribute . ' ' . $dir);
        } else {
            parent::setOrder($attribute, $dir);
        }

        return $this;
    }

    /**
     * @inheritdoc
     *
     * @return Select
     * @since 100.2.0
     */
    public function getSelectCountSql()
    {
        $countSelect = clone parent::getSelectCountSql();

        $countSelect->reset(Select::COLUMNS);
        $countSelect->columns('COUNT(DISTINCT order_items.item_id)');

        return $countSelect;
    }

    /**
     * Prepare between sql
     *
     * @param string $fieldName Field name with table suffix ('created_at' or 'main_table.created_at')
     * @param string $from
     * @param string $to
     * @return string Formatted sql string
     */
    protected function prepareBetweenSql($fieldName, $from, $to)
    {
        return sprintf(
            '(%s BETWEEN %s AND %s)',
            $fieldName,
            $this->getConnection()->quote($from),
            $this->getConnection()->quote($to)
        );
    }
}
