<?php
namespace AHT\SalesAgent\Model;

use AHT\SalesAgent\Api\Data\SalesagentInterface;

class Salesagent extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface, SalesagentInterface
{
    const CACHE_TAG = 'aht_salesagent_salesagent';

    /**
     * Model cache tag for clear cache in after save and after delete
     *
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'salesagent';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('AHT\SalesAgent\Model\ResourceModel\Salesagent');
    }

    /**
     * Return a unique id for the model.
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getEntityId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    public function getOrderId()
    {
        return parent::getData(self::ORDER_ID);
    }
    
    public function getOrderItemId()
    {
        return parent::getData(self::ORDER_ITEM_ID);
    }

    public function getOrderItemSku()
    {
        return parent::getData(self::ORDER_ITEM_SKU);
    }

    public function getOrderItemPrice()
    {
        return parent::getData(self::ORDER_ITEM_PRICE);
    }

    public function getCommissionPercent()
    {
        return parent::getData(self::COMMISSION_PERCENT);
    }

    public function getCommissionValue()
    {
        return parent::getData(self::COMMISSION_VALUE);
    }

    public function setEntityId($entity_id)
    {
        return parent::setData(self::ENTITY_ID, $entity_id);
    }

    public function setOrderId($order_id)
    {
        return parent::setData(self::ORDER_ID, $order_id);
    }
    
    public function setOrderItemId($order_item_id)
    {
        return parent::setData(self::ORDER_ITEM_ID, $order_item_id);
    }

    public function setOrderItemSku($order_item_sku)
    {
        return parent::setData(self::ORDER_ITEM_SKU, $order_item_sku);
    }

    public function setOrderItemPrice($order_item_price)
    {
        return parent::setData(self::ORDER_ITEM_PRICE);
    }

    public function setCommissionPercent($percent)
    {
        return parent::setData(self::COMMISSION_PERCENT, $percent);
    }

    public function setCommissionValue($value)
    {
        return parent::setData(self::COMMISSION_VALUE, $value);
    }
}
