<?php
namespace AHT\SalesAgent\Model\Source;

class Commissiontypedropdown extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    public function getAllOptions() {
        if ($this->_options === null) {
            $this->_options = [
                ['label' => __('--Select--'), 'value' => ''],
                ['label' => __('Percent'), 'value' => 1],
                ['label' => __('Fixed'), 'value' => 2]
            ];
        }
        return $this->_options;
    }
}
