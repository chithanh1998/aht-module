<?php
namespace AHT\SalesAgent\Model\Source;

class SaleAgentdropdown extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @param \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $collectionFactory
    
    )
    {
        # code...
        $this->collectionFactory = $collectionFactory;
    }
    public function getAllOptions() {
        $collection = $this->collectionFactory->create()->addAttributeToFilter('is_sales_agent', 1);
        if ($this->_options === null) {
            $this->_options[] = ['label' => __('--Select--'), 'value' => ''];
        }
        foreach ($collection as $item) {
            $name = $item->getFirstname() . " " . $item->getLastname();
            $this->_options[] = ['label' => __($name), 'value' => $item->getId()];
        }
        return $this->_options;
    }
}
