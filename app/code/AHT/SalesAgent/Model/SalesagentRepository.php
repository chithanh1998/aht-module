<?php

namespace AHT\SalesAgent\Model;

use AHT\SalesAgent\Api\Data;
use AHT\SalesAgent\Api\SalesagentRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use AHT\SalesAgent\Model\ResourceModel\Salesagent as SalesagentResource;
use Magento\Store\Model\StoreManagerInterface;
use AHT\SalesAgent\Model\SalesagentFactory;

class SalesagentRepository implements SalesagentRepositoryInterface
{
    protected $resource;

    protected $salesagentFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataSalesagentFactory;

    private $storeManager;

    public function __construct(
        SalesagentResource $resource,
        SalesagentFactory $salesagentFactory,
        Data\SalesagentInterfaceFactory $dataSalesagentFactory,
        DataObjectHelper $dataObjectHelper,
		DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
		$this->salesagentFactory = $salesagentFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataCategoryFactory = $dataSalesagentFactory;
		$this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    public function save(\AHT\SalesAgent\Api\Data\SalesagentInterface $salesagent)
    {
        try {
            $this->resource->save($salesagent);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save : %1', $exception->getMessage()),
                $exception
            );
        }
        return $salesagent;
    }

    public function getById($entityId)
    {
		$sale = $this->salesagentFactory->create()->load($entityId);
        if (!$sale->getId()) {
            throw new NoSuchEntityException(__('Id "%1" does not exist.', $entityId));
        }
        return $sale;
    }
	
    public function delete(\AHT\SalesAgent\Api\Data\SalesagentInterface $salesagent)
    {
        try {
            $this->resource->delete($salesagent);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    public function deleteById($Id)
    {
        return $this->delete($this->getById($Id));
    }
}
?>