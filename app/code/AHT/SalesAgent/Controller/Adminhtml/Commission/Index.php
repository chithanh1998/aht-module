<?php

namespace AHT\SalesAgent\Controller\Adminhtml\Commission;

use Magento\Reports\Model\Flag;

class Index extends \Magento\Reports\Controller\Adminhtml\Report\Sales
{
    public function execute()
    {
        $this->_initAction()->_setActiveMenu(
            'AHT_SalesAgent::commission'
        )->_addBreadcrumb(
            __('Products Commission'),
            __('Products Commission')
        );
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Products Commission Sales Agent'));
        $this->_view->renderLayout();
    }

}