<?php
namespace AHT\SalesAgent\Observer;

use AHT\SalesAgent\Model\SalesagentFactory;

class AddtoReportTable implements \Magento\Framework\Event\ObserverInterface
{
    protected $salesagentFactory;

    protected $salesagentResource;

    public function __construct(SalesagentFactory $salesagentFactory)
    {
        $this->salesagentFactory = $salesagentFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $salesagent = $this->salesagentFactory->create();
        $items = $order->getAllItems();
        foreach ($items as $item){
            $haveAgents = explode(',', $item->getProduct()->getSaleAgent());

            if(count($haveAgents)>0) {
                for($i = 0; $i<count($haveAgents); $i++){
                    $orderData = [
                        'order_id' => $order->getIncrementId(),
                        'agent_id' => $haveAgents[$i],
                        'order_item_id' => $item->getProductId(),
                        'order_item_sku' => $item->getSku(),
                        'order_item_price' => $item->getPrice(),
                        'commission_type' => $item->getProduct()->getCommissionType(),
                        'commission_value' => $item->getProduct()->getCommissionValue()
                    ];
                    $salesagent->setData($orderData);
                    $salesagent->save();
                }
            }
        }
        return true;
    }
}