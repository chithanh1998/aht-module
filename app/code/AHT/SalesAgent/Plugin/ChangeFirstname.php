<?php
namespace AHT\SalesAgent\Plugin;

class ChangeFirstname
{
    public function afterGetFirstname(\Magento\Customer\Model\Data\Customer $subject, $result)
    {
        if($subject->getCustomAttribute("is_sales_agent") && $subject->getCustomAttribute("is_sales_agent")->getValue() == \Magento\Eav\Model\Entity\Attribute\Source\Boolean::VALUE_YES){
            $result = "Sales Agent: " .$result;
        }
        return $result;
    }
}
