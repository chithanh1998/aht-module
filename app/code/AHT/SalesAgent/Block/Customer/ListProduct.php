<?php
namespace AHT\SalesAgent\Block\Customer;

class ListProduct extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    private $collection;

    /**
     * @param \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collection,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    ) {
        $this->collection = $collection;
        $this->customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        if ($this->getProduct()) {
            $toolbar = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'customer_sale_agent.toolbar'
            )->setCollection(
                $this->getProduct()
            );

            $this->setChild('toolbar', $toolbar);
        }
        return parent::_prepareLayout();
    }

    public function getProduct()
    {
        $id = $this->customerSession->getCustomer()->getId();
        $limit = $this->getRequest()->getParam('limit') ? $this->getRequest()->getParam('limit') : 10;
        $page = $this->getRequest()->getParam('p') ? $this->getRequest()->getParam('p') : 1;
        $product = $this->collection->create()
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('sale_agent', $id)->setPageSize($limit)->setCurPage($page);
        return $product;
    }

    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }
}
