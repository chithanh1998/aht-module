<?php
namespace AHT\KnockOutJs\Controller\Test;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Helper\Image;
use Magento\Store\Model\StoreManager;

class Product extends \Magento\Framework\App\Action\Action
{
   protected $collectionFactory;
   protected $imageHelper;
   protected $listProduct; 
   protected $_storeManager;
   protected $priceCurrency;

   public function __construct(
       \Magento\Framework\App\Action\Context $context, 
       \Magento\Framework\Data\Form\FormKey $formKey,
       CollectionFactory $collectionFactory,
       StoreManager $storeManager,
       Image $imageHelper,
       \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
   )
   {
       $this->collectionFactory = $collectionFactory;
       $this->imageHelper = $imageHelper;
       $this->_storeManager = $storeManager; 
       $this->priceCurrency = $priceCurrency;
       parent::__construct($context);
   }

   public function execute()
   {
        if ($name = $this->getRequest()->getParam('name')) {
            $collection = $this->collectionFactory->create();
            try {
                $collection->addAttributeToSelect('*')->addAttributeToFilter('name', array('like' => '%'.$name.'%'))->setPageSize(10);
                foreach ($collection as $item) {
                    $productData[] = [
                        'entity_id' => $item->getId(),
                        'name' => $item->getName(),
                        'price' => $item->getPrice(),
                        'src' => $this->imageHelper->init($item, 'product_base_image')->getUrl(),
                        'symbol' => $this->priceCurrency->getCurrencySymbol(),
                    ];
                }
                echo json_encode($productData);
            } catch (\Throwable $th) {
                throw new \Exception('No result');
            }
            
        }
    }
}
               