define([
    'ko',
    'uiComponent',
    'mage/url',
    'mage/storage',
    'jquery',
    'Magento_Customer/js/customer-data'
 ], function (ko, Component, urlBuilder,storage, $, customerData) {
    'use strict';

    function product(item, symbol) {
        item.getPrice = function () {
            return item.price;
        }
        item.symbol = symbol;

        return item;

    }

    function viewModel(){
        var self = this;
        self.searchList = ko.observableArray([]);
        self.productList = ko.observableArray([]);
        self.symbol = ko.observable();

        self.countLine = ko.computed(function () {
            return self.productList().length;
        })

        self.countQty = ko.computed(function () {
            var totalQty = 0;
            ko.utils.arrayFilter(self.productList(), function (product) {
                totalQty += product.qty();
            });
            return totalQty;
        })

        self.subTotal = ko.computed(function () {
            var total = 0;
            ko.utils.arrayFilter(self.productList(), function (product) {
                total += product.total();
            });
            return total;
        })

        function productList(item) {
            var self = this;
            self.symbol = item.symbol;
            self.product = ko.observable(product(item, item.symbol));
            self.qty = ko.observable(1);
            self.qtyUp = function () {
                self.qty(self.qty() + 1);
            };
            self.qtyDown = function () {
                if (self.qty() > 1) {
                    self.qty(self.qty() - 1);
                }
            };
            self.name = item.name;
            self.sku = ko.observable(item.sku);
            self.getId = item.entity_id;
            self.src = item.src;
            self.total = ko.computed(function () {
                return self.qty() * self.product().getPrice();
            });
        }

        self.getProduct = function(){
            var product = $('.product-name').val();
            if(product){
                var serviceUrl = urlBuilder.build('knockout/test/product?name='+product);
                return storage.get(
                    serviceUrl,
                    ''
                ).done(
                    function (response) {
                        var items = JSON.parse(response);
                        console.log(items);
                        var product = $.map(items, function (item) {
                            item['isCheck'] = ko.observable(self.checkExistsInTable(item));
                            return item;
                        })
                        self.searchList(product);
                    }
                ).fail(
                    function (response) {
                        console.log(response);
                    }
                );
            } else {
                self.searchList([]);
            }
        }

        self.checkExistsInTable = function (item) {
            var exist = false;
            var idProducSearch = item.entity_id;
            ko.utils.arrayFilter(self.productList(), function (product) {
                if (product.getId == idProducSearch) {
                    exist = true;
                }
            });
            console.log(exist)
            return exist;
        }

        self.delete = function (item) {
            self.productList.remove(item);
            ko.utils.arrayFilter(self.searchList(), function (product) {
                if (product.entity_id == item.getId) {
                    product.isCheck(false);
                }
            });
        }

        self.check = function (item) {

            var exist = false;
            var idProducSearch = item.entity_id;
            var productExists = false;
            ko.utils.arrayFilter(self.productList(), function (product) {
                console.log(product)
                if (product.getId == idProducSearch) {
                    exist = true;
                    productExists = product;
                }
            });

            if (!exist && item.isCheck()) {
                self.productList.push(new productList(item, self.symbol()));
            } else if (exist && !item.isCheck() && productExists) {
                self.productList.remove(productExists);
            }

        }

        self.addtocart = function () {
            var serviceUrl = urlBuilder.build('knockout/test/addtocart');
            var data = [];

            ko.utils.arrayFilter(self.productList(), function (product) {
                data.push({
                    'product': product.getId,
                    'qty': product.qty()
                })
            });

            storage.post(
                serviceUrl,
                JSON.stringify(data),
                false
            ).done(
                function (response, status) {
                    if (status == 'success') {
                        alert("Success");
                        // location.reload();
                        self.productList([]);
                        self.searchList([]);
                        customerData.reload(['cart'], true);
                        $('.product-name').val('');
                    }
                }
            ).fail(function () {
                alert('add cart fail');
            });
        }
    }
    return Component.extend(new viewModel());
 });